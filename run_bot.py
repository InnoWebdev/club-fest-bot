import json

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters, RegexHandler

import logging

# Enable logging
from storage import save_score

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

IS_PLAYING, FIRST_ANSWER, LOCATION, BIO = range(4)

questions = [
    # 1
    {
        'question': 'Question 1. Can you use C++ for frontend development?',
        'answers': [
            '12', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    },
    # 2
    {
        'question': 'Question 2. Should you filter query results in browser?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'No'
    },
    # 3
    {
        'question': 'Question 3. Is it possible to create a backend for a project using only nginx?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    },
    # 4
    {
        'question': 'Question 4. Can you run Microsoft IIS in a Docker container?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    },
    # 5
    {
        'question': 'Question 5. Do people use DNS servers as load balancers?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    },
    # 6
    {
        'question': 'Question 6. Serverless cloud architecture - does it provide lower latency?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'No'
    },
    # 7
    {
        'question': 'Question 7. Is it a good idea to use MD5 hashes for password storage?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'No'
    },
    # 8
    {
        'question': 'Question 8. Do you really need HTTPS for static web pages??',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    },
    # 9
    {
        'question': 'Question 9. Can you use a single prepared query over several Postgres connections?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'No'
    },
    # 10
    {
        'question': 'Question 10. [1] + 1 == 11?',
        'answers': [
            '2', '15', '16', '4'
        ],
        'right_answer': 'Yes'
    }
]

current_players = {}  # storage for current players


def start(bot, update):
    reply_keyboard = [['Yes']]

    update.message.reply_text(
        'Hi! I am webdev clubfest bot. I will help you with quiz. '
        'Send /cancel to stop talking to me.\n\n'
        'Let\'s get started?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return IS_PLAYING


def start_quiz(bot, update):
    user = update.message.from_user
    logger.info("%s is going to play: %s", user.first_name, update.message.text)
    _question_helper(0, bot, update)

    return FIRST_ANSWER


def _question_helper(current, bot, update):
    reply_keyboard = [['Yes'], ['No']]
    user = update.message.from_user

    if current == 0:
        current_players[user.id] = 0

    logger.info("%s going to answer %d question", user.first_name, current)
    if current < 10:
        update.message.reply_text(questions[current]['question'],
                                  reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))


def question1(bot, update):
    current = 1

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question2(bot, update):
    current = 2

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question3(bot, update):
    current = 3

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question4(bot, update):
    current = 4

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question5(bot, update):
    current = 5

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question6(bot, update):
    current = 6

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question7(bot, update):
    current = 7

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question8(bot, update):
    current = 8

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question9(bot, update):
    current = 9

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    _question_helper(current, bot, update)

    return current + 1


def question10(bot, update):
    current = 10

    user = update.message.from_user
    prev_user_answer = update.message.text
    if questions[current - 1]['right_answer'] == prev_user_answer:
        current_players[user.id] = current_players[user.id] + 1

    logger.info("%s anwered the last question", user.first_name)

    update.message.reply_text(f'Bye! I hope we can talk again some day. You have {current_players[user.id]} out of 10.',
                              reply_markup=ReplyKeyboardRemove())

    save_score(user, current_players[user.id])

    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token='666978603:AAEFHc_kdlPgR2BMik2rCBk3VsJUohlf0kU')

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            IS_PLAYING: [RegexHandler('^(Yes)$', start_quiz)],

            1: [MessageHandler(Filters.text, question1)],
            2: [MessageHandler(Filters.text, question2)],
            3: [MessageHandler(Filters.text, question3)],
            4: [MessageHandler(Filters.text, question4)],
            5: [MessageHandler(Filters.text, question5)],
            6: [MessageHandler(Filters.text, question6)],
            7: [MessageHandler(Filters.text, question7)],
            8: [MessageHandler(Filters.text, question8)],
            9: [MessageHandler(Filters.text, question9)],
            10: [MessageHandler(Filters.text, question10)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
