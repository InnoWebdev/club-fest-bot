import json
import logging
from json import JSONDecodeError

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def save_score(user, score):
    user.score = score

    with open('data/data.json', 'r') as json_file:
        try:
            data = json.load(json_file)
        except JSONDecodeError:
            data = []
        logger.info(data)

    import time
    timestamp = str(int(time.time() * 1000.0))
    with open(f'data/data.json', 'w') as json_file:

        is_added = False
        for s_user in data:
            if user.username == s_user['username']:
                s_user['score'] = score
                is_added = True

        user_to_save = {
            'score': score,
            'username': user.username
        }

        if not is_added:
            data.append(user_to_save)

        logger.info(data)

        json.dump(data, json_file)
