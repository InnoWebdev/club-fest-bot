var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');


app.use(express.static(path.join(__dirname, '/')));

app.get('/', function(req, res){
    res.sendfile(__dirname + '/index.html');
});

io.on('connection', function(socket){

    socket.emit('connected', { 'id': socket.id });

    socket.on('update', function(msg){
        //console.log(msg);
        io.emit('update', msg);
    });

});

http.listen(8080, "0.0.0.0", function(){
    console.log('listening on *:8080');
});
